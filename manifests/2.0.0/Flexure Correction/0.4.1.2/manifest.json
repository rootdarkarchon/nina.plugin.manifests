﻿{
    "MinimumApplicationVersion": {
        "Minor": "0",
        "Build": "2050",
        "Patch": "0",
        "Major": "2"
    },
    "Installer": {
        "ChecksumType": "SHA256",
        "URL": "https://bitbucket.org/fmeschia/drift-correction-plugin/downloads/Fmeschia.NINA.Plugin.DriftCorrection-0.4.1.2.dll",
        "Checksum": "EE8D42BB878931D1E10FEDBAD41249E96F67BECB730EFE92541ECDBB3F2BFE8F",
        "Type": "DLL"
    },
    "Version": {
        "Minor": "4",
        "Build": "2",
        "Patch": "1",
        "Major": "0"
    },
    "Tags": [
        "Sequencer",
        "Trigger",
        "Drift Corrector",
        "Drift Correction",
        "Flexure",
        "Differential Flexure",
        "Flexure Correction",
        "Flexure Corrector"
    ],
    "Descriptions": {
        "ShortDescription": "A plugin that evaluates and corrects drift due to differential flexure.",
        "LongDescription": "**What is differential flexure?**\r\n\r\nWhen taking guided exposures, the guiding system will do what’s necessary to keep the stars in the same positions in its field of view.\r\nThis works on the assumption that the field of view of the guiding system and of the imaging scope are in a fixed relationship with one another, so that guiding a star in one will result in round star images in the other.\r\n\r\nUnfortunately, this is not always true. When using separate guide and imaging scopes, their mounting is usually sufficient to keep them *approximately* in a fixed relationship, but not always in a precise way. \r\nIf the guide scope mounting system, for instance, flexes a bit while an exposure is taken, the guiding system will “think” the stars are being tracked very precisely, while they are \r\nactually producing short trails on the imaging sensor attached to the main scope. This is *differential flexure*: a difference in the way the guide scope and the imaging scope respond to gravity and movement over time.\r\n\r\nIf you use a guide scope, you *will* have some degree of differential flexure. This plugin aims at correcting and reducing its adverse effects. It does so by learning how the alignment of the two scopes changes over time, \r\nand then instructing the guider to introduce a gradual shift in the lock position to compensate for it.\r\n\r\nTo use this plugin, add the \"Flexure Correction\" trigger into your sequence. The trigger will detect when an exposure is about to start, and before that happens it will take a short exposure (the duration is \r\ndetermined by NINA’s plate solving settings). Filter and focus position will be the same used by the exposure that’s about to start. This image will be plate solved and compared with the result of the actual \r\nexposure to determine how much the imaging scope has drifted during the duration of the exposure. The drift will be used to estimate the rate of drift of the imaging scope in arcseconds per hour, and the guider \r\nwill be instructed to shift its lock point with the same rate, but in the opposite direction, to reduce the overall drift in the next image.\r\n\r\nThe plugin can be configured to ignore drifts that are either too small (e.g. within the circle of uncertainty due to seeing), or too large (e.g. due to sudden \"mirror flop\"). These limits can be set in the \r\nplugin options page, and expressed in units of guiding RMS: for instance, the default setting is to ignore drifts that have affected the exposure by less than its guiding RMS, or by more than 10 times the guiding RMS. \r\nYou can also specify that only exposures longer than a certain duration are to be considered for shift rate calculation, for a more accurate projection of the overall rate of shift per hour. \r\nThis can be set at a global level, or in each instance of the trigger in the sequence. An \"aggressiveness\" parameter can also be set, to determine how much of the measured drift in each exposure will be considered \r\nto calculate the new shift rate."
    },
    "Name": "Flexure Correction",
    "LicenseURL": "https://www.mozilla.org/en-US/MPL/2.0/",
    "Author": "Francesco Meschia",
    "Repository": "https://bitbucket.org/fmeschia/drift-correction-plugin",
    "License": "MPL-2.0",
    "Identifier": "A4727186-A52B-408F-BC2A-BF3E1C095BCF"
}
