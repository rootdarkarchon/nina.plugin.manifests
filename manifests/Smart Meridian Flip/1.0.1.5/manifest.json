{
    "MinimumApplicationVersion": {
        "Minor": "0",
        "Build": "2050",
        "Patch": "0",
        "Major": "2"
    },
    "Installer": {
        "ChecksumType": "SHA256",
        "URL": "https://bitbucket.org/fmeschia/smartmeridianflip/downloads/Fmeschia.NINA.Plugin.SmartMeridianFlip-1.0.1.5.dll",
        "Checksum": "33E32018AF81DBB0AD22B5CFF768A4DBE87EB3C211EF575A983D4C767A77E13F",
        "Type": "DLL"
    },
    "Version": {
        "Minor": "0",
        "Build": "5",
        "Patch": "1",
        "Major": "1"
    },
    "Tags": [
        "Sequencer",
        "Trigger",
        "Meridian Flip",
        "Declination",
        "Meridian Delay",
        "Meridian Pause"
    ],
    "Descriptions": {
        "ShortDescription": "A meridian flip trigger that considers obstructions at different declinations to optimize imaging downtime.",
        "LongDescription": "The Smart Meridian Flip trigger can enforce different safe tracking limits for the West and East side of the meridian at different declinations.\r\n\r\nThis can be useful to optimize the imaging time when, for instance, the mount needs to stop tracking before the meridian to prevent the scope from hitting the pier, but only for certain values of declination(typically where the back of the scope is closer to the pier / tripod).\r\n\r\nTo use this plugin, configure it and insert it as a sequence trigger in the Advanced Sequencer.\r\n\r\nThis plugin needs to be configured with a file that describes the obstructions present on the West and East side at various declination angles. It can process Astro - Physics APCC `.mlm` files directly, or it can process `.obs` files that follow this format:\r\n\r\n    \u003c declination 1 \u003e\u003c West limit in minutes \u003e\u003c East limit in minutes \u003e\r\n    \u003c declination 2 \u003e\u003c West limit in minutes \u003e\u003c East limit in minutes \u003e\r\n    ...\r\n    \u003c declination n \u003e\u003c West limit in minutes \u003e\u003c East limit in minutes \u003e\r\n\r\nThe West - side and East - side limits are in minutes, and they indicate the maximum time before the meridian(West - side) and minimum time after the meridian(East - side) that it\u0027s safe for the telescope to be at, for a given declination. \r\nThe West-side obstruction can also be a negative value, to indicate how far the telescope can safely track past the meridian.\r\n\r\nSince a meridian flip is possible both on the arc of meridian between the Pole and the horizon opposite to the Pole(\"above\" the Pole), and on the arc between the Pole and the horizon next to the Pole(\"below\" the Pole), the declination range used in the file is expanded from the traditional -90 - +90 degree range. \r\n\r\nValues between - 90 degrees and 90 degrees will be used to represent the arc of meridian \"above\" the Pole, and values between 90 and 180 degrees will be used to represent the arc \"below\" the Pole. So, 85 degrees of celestial declination on the arc from the Pole to the horizon next to it will be represented in the file by 95 degrees(180 - 85), 60 degrees of celestial declination will be represented by 120 degrees (180 - 60), and so on.\r\n\r\nThe file must contain at least two lines, corresponding to different declinations. Declination values between listed values will be interpolated.\r\n\r\nAn example of an obstructions file:\r\n\r\n    # File Example\r\n    # Any line starting with \u0027#\u0027 is a comment\r\n    #\r\n    # The following line means that, at declination zero, the telescope can track 10 minutes after meridian, but can \r\n    # also safely flip right after the meridian\r\n    0 - 10 0\r\n    # This line says that at dec. +40 deg the telescope can safely track up to the meridian, and flip right after \r\n    # the target has passed the meridian\r\n    40 0 0\r\n    # This line says that at dec. +45 deg the telescope should stop tracking at least 20 minutes before\r\n    # the meridian, can can flip no earlier than 30 minutes after the target has crossed the meridian\r\n    45 20 30\r\n    # This line says that at dec. +50 deg the telescope can again safely track up to the meridian, and can flip\r\n    # right after the target has crossed the meridian\r\n    50 0 0\r\n    # The same limits continue to the pole:\r\n    90 0 0\r\n    # And to the equator \"below\" the pole:\r\n    180 0 0\r\n\r\n\r\n"
    },
    "Name": "Smart Meridian Flip",
    "LicenseURL": "https://www.mozilla.org/en-US/MPL/2.0/",
    "Author": "Francesco Meschia",
    "Repository": "https://bitbucket.org/fmeschia/smartmeridianflip/",
    "License": "MPL-2.0",
    "Identifier": "6d0e07f2-8773-4229-bf2c-f451e53f677a"
}
